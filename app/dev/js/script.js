jQuery(function($){

    $('.js-nav-open').on('click',function(){
        $('.nav').addClass('active');
        $('.js-nav-bg').fadeIn('400');
    });
    
    $('.js-nav-close').on('click',function(){
        $('.nav').removeClass('active');
        $('.js-nav-bg').fadeOut('400');
    });

    $(window).on('load resize',function(){
        $('.nav').removeClass('active');
        $('.js-nav-bg').fadeOut('400');
        $('.about').removeClass('active');
    });


    var $win = $(window);

    $win.on('load resize', function() {
    var windowWidth = window.innerWidth;

    if (windowWidth > 1024) {

        $('.js-about-open').on('click',function(){
            $('.about').addClass('active');
            $('.js-nav-bg').fadeIn('400');
            return false;
        });

        $('.js-about-close').on('click',function(){
            $('.about').removeClass('active');
            $('.js-nav-bg').fadeOut('400');
            return false;
        });

    }  else {

        $('.js-about-open').on('click',function(){
            $('.about').addClass('active');
            return false;
        });

        $('.js-about-close').on('click',function(){
            $('.about').removeClass('active');
            return false;
        });

    }

    });

    var swiper;

        function swiperFunc() {
            swiper = new Swiper('.swiper-container', {
                //オプションをここに:
                mode: 'horizontal',
                loop: true,
                slidesPerView: 1,
                spaceBetween: 0,
                autoHeight: true,
                effect: 'fade',
                autoplay: { delay: 3000 },
                speed: 1000
            });
        }
        $(window).on("load", function () {
            swiperFunc();
        });

        $('.keyvisiul').on("click", function () {
            $('.wrapper').show(function () {
                $('.keyvisiul').addClass('active');
                $(this).addClass('active');
            })
        });

    $(".keyvisiul").mousewheel(function (eo, delta, deltaX, deltaY) {
        $('.wrapper').show(function () {
            $('.keyvisiul').addClass('active');
            $(this).addClass('active');
        })
    });

    var speed = 30;
    $('.collection__list').mousewheel(function(eo, delta, deltaX, deltaY) {
        //ie firefox
        $(this).scrollLeft($(this).scrollLeft() - delta * speed);
        console.log(delta);
        //webkit

        return false;   //縦スクロール不可
    });

    $('.js-collection__title').on('click',function(){
        // $(this).next('.collection__list').scrollLeft(0);
        $(this).next('.collection__list').animate({
            scrollLeft : 0
        },400);
        return false;
    })

    $(".js-fade").on("inview", function (event, isInView) {
        if (isInView) {
            $(this).addClass("active");
        }
    })

    $(".js-scroll").on("inview", function (event, isInView) {
        if (isInView) {
            $(this).addClass("active");
        }
    })

})